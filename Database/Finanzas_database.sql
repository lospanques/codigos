##### SCHEMA #####
DROP DATABASE if exists Finanzas;
CREATE DATABASE Finanzas;

USE Finanzas;

###### Tablas #####
DROP TABLE if exists productos;
DROP TABLE if exists usuarios;

DROP TABLE if exists catalogo_status;
DROP TABLE if exists Catalogo_tipo_transaccion;
DROP TABLE if exists Catalogo_frecuencia;
DROP TABLE if exists Catalogo_tipo_producto;

##### Catálogos #####
CREATE TABLE catalogo_status (
	status_id int NOT NULL AUTO_INCREMENT,
	status varchar(255) NOT NULL,
	PRIMARY KEY (status_id)
);

CREATE TABLE Catalogo_tipo_producto (
	tipo_prod_id int NOT NULL,
	descripcion varchar(255) NOT NULL,
    PRIMARY KEY (tipo_prod_id)
);

CREATE TABLE Catalogo_tipo_transaccion (
	tipo_id int NOT NULL AUTO_INCREMENT,
	tipo varchar(255) NOT NULL,
	PRIMARY KEY (tipo_id)
);

CREATE TABLE Catalogo_frecuencia (
	frecuencia_id int NOT NULL AUTO_INCREMENT,
	frecuencia varchar(255) NOT NULL,
	PRIMARY KEY (frecuencia_id)
);

##### listado Usuarios y Productos #####

CREATE TABLE usuarios (
	usuario_id int NOT NULL AUTO_INCREMENT,
	nombre varchar(255) NOT NULL,
	apellido varchar(255) NOT NULL,
	apodo varchar(255),
	correo_principal varchar(255),
	correo_respaldo varchar(255) NOT NULL,
	PRIMARY KEY (usuario_id)
);

CREATE TABLE productos (
	producto_id int NOT NULL AUTO_INCREMENT,
	usuario_id int NOT NULL,
	tipo_prod_id int NOT NULL,
	status_id int NOT NULL,
	nombre varchar(255) NOT NULL,
	descripcion varchar(255),
	observaciones varchar(255),
	PRIMARY KEY (producto_id),
    CONSTRAINT fk_usuario
    FOREIGN KEY (usuario_id)
		REFERENCES usuarios(usuario_id),
	CONSTRAINT fk_tipo_prod_id
    FOREIGN KEY (tipo_prod_id)
		REFERENCES Catalogo_tipo_producto(tipo_prod_id),
	CONSTRAINT fk_status_id
    FOREIGN KEY (status_id)
		REFERENCES catalogo_status(status_id)
        ON UPDATE CASCADE
        ON DELETE CASCADE
);
